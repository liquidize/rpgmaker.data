﻿// MIT License
//
// Copyright (c) 2016 Daniel Deptula
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
using System.Collections.Generic;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
    /// <summary>
    /// An Actor object. An Actor is any character in the game that can battle, or be used to walk around and interact with an event.
    /// Actors have traits, equipment, and parameters that give them stats or abilities.
    /// </summary>
    public class Actor
    {
        /// <summary>
        /// Id of the actor.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        /// <summary>
        /// The image of the actors battler sprite.
        /// </summary>
        [JsonProperty(PropertyName = "battlerName")]
        public string BattlerName { get; set; }

        /// <summary>
        /// The index used to determine the character sprite from the character image.
        /// </summary>
        [JsonProperty(PropertyName = "characterIndex")]
        public int CharacterIndex { get; set; }

        /// <summary>
        /// The character image used to display the actor on maps.
        /// </summary>
        [JsonProperty(PropertyName = "characterName")]
        public string CharacterName { get; set; }

        /// <summary>
        /// Id of the actors starting <see cref="Class"/>.
        /// </summary>
        [JsonProperty(PropertyName = "classId")]
        public int ClassId { get; set; }

        /// <summary>
        /// The list of equipment the actor starts out with. The list contains the Id's of the equipment.
        /// Equipment Id's can be either Id's of <seealso cref="Weapon"/> or of <seealso cref="Armor"/>
        /// </summary>
        [JsonProperty(PropertyName = "equips")]
        public List<int> Equips { get; set; } = new List<int>();

        /// <summary>
        /// The index used to determine the actors face, on the face sheet image.
        /// </summary>
        [JsonProperty(PropertyName = "faceIndex")]
        public int FaceIndex { get; set; }

        /// <summary>
        /// The image used to display the actors face in menus.
        /// </summary>
        [JsonProperty(PropertyName = "faceName")]
        public string FaceName { get; set; }

        /// <summary>
        /// A list conatining <seealso cref="Trait"/>'s the actor has.
        /// </summary>
        [JsonProperty(PropertyName = "traits")]
        public List<Trait> Traits { get; set; } = new List<Trait>();

        /// <summary>
        /// The Initial Level of the actor. The level at which the actor starts.
        /// </summary>
        [JsonProperty(PropertyName = "initialLevel")]
        public int InitialLevel { get; set; }

        /// <summary>
        /// The Maximum Level the actor can obtain.
        /// </summary>
        [JsonProperty(PropertyName = "maxLevel")]
        public int MaxLevel { get; set; }
        
        /// <summary>
        /// The actors Name. 
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// The actors Nickname.
        /// </summary>
        [JsonProperty(PropertyName = "nickname")]
        public string Nickname { get; set; }

        /// <summary>
        /// The actors "note" object, used for plugins and just general developer notes.
        /// </summary>
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        /// <summary>
        /// The actors Profile, a brief description about them.
        /// </summary>
        [JsonProperty(PropertyName = "profile")]
        public string Profile { get; set; }

        /// <summary>
        /// Additional Properties, that are not apart of the MV data by default. Keyed by their name, the value is turned into a general .NET object.
        /// </summary>
        public Dictionary<string,object> Properties { get; set; } = new Dictionary<string, object>();

        /// <summary>
        /// Returns a formatted string containing the values of this actor object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string str =
                string.Format(
                    "Id: {0}, BattlerName: {1}, CharacterIndex: {2}, CharacterName: {3}, ClassId: {4}, Equips: {5}, FaceIndex: {6}, FaceName: {7}, " +
                    "Traits: {8}, InitialLevel: {9}, MaxLevel: {10}, Name: {11}, NickName: {12}, Note: {13}, Profile: {14}", Id,
                    BattlerName, CharacterIndex, CharacterName, ClassId, string.Join(",",Equips),FaceIndex,FaceName,string.Join(",",Traits),InitialLevel,MaxLevel,Name,
                    Nickname,Note,Profile);
            foreach (KeyValuePair<string, object> kvp in Properties)
            {
                str += string.Format("{0}: {1},", kvp.Key, kvp.Value);
            }
            if (str.EndsWith(","))
            {
                return str.Substring(0, str.Length - 1);
            }
            return str;
        }
    }
}
