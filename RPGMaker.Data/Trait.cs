﻿// Copyright (c) 2016 Daniel Deptula
using Newtonsoft.Json;

namespace RPGMaker.Data
{
    public class Trait
    {
        /// <summary>
        /// Code of this Trait.
        /// TODO: Make it easier to determine what the trait actually is.
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public int Code { get; set; }

        /// <summary>
        /// Data Id of this trait.
        /// </summary>
        [JsonProperty(PropertyName = "dataId")]
        public int DataId { get; set; }

        /// <summary>
        /// Value of this Trait.
        /// </summary>
        [JsonProperty(PropertyName = "value")]
        public double Value { get; set; }
    }
}
