﻿// Copyright (c) 2016 Daniel Deptula
using System.Collections.Generic;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
    /// <summary>
    /// An Animation object containing data about an MV animation. An animation is sequence of images played back with different effects via
    /// <see cref="AnimationTiming"/>. An animation is made up of multiple <see cref="AnimationCell"/>.
    /// </summary>
    public class Animation
    {
        /// <summary>
        /// Id of the Animation.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Hue of the first image used in this Animation.
        /// </summary>
        [JsonProperty(PropertyName = "animation1Hue")]
        public int Animation1Hue { get; set; }

        /// <summary>
        /// Name of the first image used in this animation.
        /// </summary>
        [JsonProperty(PropertyName = "animation1Name")]
        public string Animation1Name { get; set; }

        /// <summary>
        /// Hue of the second image used in this animation.
        /// </summary>
        [JsonProperty(PropertyName = "animation2Hue")]
        public int Animation2Hue { get; set; }

        /// <summary>
        /// Name of the second image used in this animation.
        /// </summary>
        [JsonProperty(PropertyName = "animation2Name")]
        public string Animation2Name { get; set; }

        /// <summary>
        /// List of Frames used in this Animation. The frames contain a List of <seealso cref="AnimationCell"/>s.
        /// </summary>
        [JsonProperty(PropertyName = "frames")]
        public List<List<AnimationCell>> Frames { get; set; }

        /// <summary>
        /// Name of this Animation.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Position of this Animation when played.
        /// </summary>
        [JsonProperty(PropertyName = "position")]
        public int Position { get; set; }

        /// <summary>
        /// A list of <seealso cref="AnimationTiming"/>s used in this animation to create effects and sounds.
        /// </summary>
        [JsonProperty(PropertyName = "timings")]
        public List<AnimationTiming> Timings { get; set; }

        /// <summary>
        /// Additional Proprties found in this animation that are custom and defined by users. Keyed by the name of the property. The value is a generic .NET Object
        /// which will need to be converted to the propert object type.
        /// </summary>
        public Dictionary<string,object> Properties { get; set; } = new Dictionary<string, object>();

        /// <summary>
        /// Returns a formatted string with the values of this animation.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string timings = "";
            foreach (AnimationTiming timing in Timings)
            {
                if (timing != null)
                timings += timing.ToString() + ",";
            }

            if (timings.EndsWith(","))
            {
                timings = timings.Substring(0, timings.Length - 1);
            }

            string frames = "";
            foreach (List<AnimationCell> cells in Frames)
            {
                if (cells != null)
                {
                    frames += "{";
                    foreach (AnimationCell cell in cells)
                    {
                        frames += cell.ToString();
                    }
                    frames += "},";
                }
                else
                {
                    frames += "{[]},";
                }
            }
            if (frames.EndsWith(","))
            {
                frames = frames.Substring(0, frames.Length - 1);
            }
            
            string str = string.Format("Id: {0}, Animation1Hue: {1}, Animation1Name: {2}, Animation2Hue: {3}," +
                                       "Animation2Name: {4}, Frames: {5}, Name: {6}, Position: {7}, Timings: {8}",Id,Animation1Hue,Animation1Name,Animation2Hue,
                                       Animation2Name,frames,Name,Position,timings);

            return str;
        }
    }
}
