﻿// Copyright (c) 2016 Daniel Deptula
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
#if LOG_ERRORS
using log4net;
#endif

namespace RPGMaker.Data
{
    public static class DataManager
    {
        /// <summary>
        /// List of all currently loaded Actors.
        /// </summary>
        public static List<Actor> Actors { get; set; } = new List<Actor>();

        /// <summary>
        /// List of all currently loaded Animations.
        /// </summary>
        public static List<Animation> Animations { get; set; } = new List<Animation>();

        /// <summary>
        /// List of all currently loaded Armors.
        /// </summary>
        public static List<Armor> Armors { get; set; } = new List<Armor>();

        /// <summary>
        /// List of all currently loaded Classes.
        /// </summary>
        public static List<Class> Classes { get; set; } = new List<Class>();

        /// <summary>
        /// List of all currently loaded Enemies.
        /// </summary>
        public static List<Enemy> Enemies { get; set; } = new List<Enemy>();

        /// <summary>
        /// List of all currently loaded Items.
        /// </summary>
        public static List<Item> Items { get; set; } = new List<Item>();

        /// <summary>
        /// List of all currently loaded <see cref="Skill"/> objects.
        /// </summary>
        public static List<Skill> Skills { get; set; } = new List<Skill>();

        /// <summary>
        /// <see cref="List{T}"/> of all currently loaded <see cref="State"/> objects.
        /// </summary>
        public static List<State> States { get; set; } = new List<State>();

        /// <summary>
        /// <see cref="List{Tileset}"/> of all currently loaded <see cref="Tileset"/> objects. 
        /// </summary>
        public static List<Tileset> Tilesets { get; set; } = new List<Tileset>();

        /// <summary>
        /// <see cref="List{Weapon}"/>  of all currently loaded <see cref="Weapon"/> objects. 
        /// </summary>
        public static List<Weapon> Weapons { get; set; } = new List<Weapon>();

        /// <summary>
        /// A true/false value indicatiog whether or not the API handles possible known exceptions by default.
        /// By default, exceptions are handled.
        /// </summary>
        public static bool HandleExceptions { get; set; } = true;

#if LOG_ERRORS
        /// <summary>
        /// Log used to log errors.
        /// </summary>
        private static ILog log = log4net.LogManager.GetLogger("DataManager");
#endif
        /// <summary>
        /// Array of strings containing the property names of all the default Actor properties used by the engine.
        /// </summary>
        private static readonly string[] defaultActorProperties = new string[]
        {
            "id", "battlerName", "characterIndex", "characterName",
            "classId", "equips", "faceIndex", "faceName", "initialLevel",
            "maxLevel", "name", "nickname", "note", "profile", "traits"
        };

        /// <summary>
        /// Array of string containing all the default property names of the Animations.
        /// </summary>
        private static readonly string[] defaultAnimationProperties = new string[]
        {
            "id", "animation1Hue", "animation1Name", "animation2Hue",
            "animation2Name", "name", "frames", "position", "timings"
        };

        /// <summary>
        /// Array of strings containing the property names of all the default Armor properties used by the engine.
        /// </summary>
        private static readonly string[] defaultArmorProperties = new string[]
        {
            "id", "atypeId", "description","etypeId", "traits", "iconIndex",
            "name", "note", "params", "price"
            
        };

        private static readonly string[] defaultClassProperties = new string[]
        {
            "id","expParams","traits","learnings","name","note","params"
        };

        private static readonly string[] defaultEnemyProperties = new string[]
        {
            "id", "actions", "battlerHue", "battlerName", "dropItems", "exp",
            "traits", "gold", "name", "note", "params"
        };

        private static readonly string[] defaultItemProperties = new string[]
        {
            "id", "animationId", "consumable", "damage", "description", "effects",
            "hitType", "iconIndex", "itypeId", "name", "note", "occasion", "price",
            "repeats", "scope", "speed", "successRate", "tpGain"
        };

        private static readonly string[] defaultSkillProperties = new string[]
        {
             "id", "animationId", "damage", "description", "effects",
             "hitType", "iconIndex", "stypeId", "name", "note", "occasion", "mpCost",
             "repeats", "scope", "speed", "successRate", "tpGain","tpCost","requiredWtypeId1",
             "requiredWtypeId2","message1","message2"
        };

        private static readonly string[] defaultStateProperties = new string[]
        {
            "id","autoRemovalTiming","chanceByDamage","iconIndex","maxTurns","message1",
            "message2","message3","message4","minTurns","motion","name","note","overlay",
            "priority","releaseByDamage","removeAtBattleEnd","removeByRestriction","removeByWalking",
            "restriction","stepsToRemove","traits","description"
        };

        private static readonly string[] defaultTilesetProperties = new string[]
        {
            "id","flags","mode","name","note","tilesetNames"
        };

        private static readonly string[] defaultWeaponProperties = new string[]
        {
            "id", "wtypeId", "description","etypeId", "traits", "iconIndex",
            "name", "note", "params", "price"

        };

        /// <summary>
        /// Load an actors file. The actors contained in this file are loaded into the DataManager.Actors list.
        /// The list is cleared before loading the file/
        /// </summary>
        /// <param name="file">json file containing the actors data.</param>
        /// <returns>The amount of actors objects loaded, or -1 if an error occurs.</returns>
        public static int LoadActors(string file)
        {
            // Clear the currently loaded actors.
            Actors.Clear();
            if (!File.Exists(file))
            {
#if LOG_ERRORS
                log.ErrorFormat("Failed to load Actors. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load Actors. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

                JArray actorArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in actorArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    Actor actor = new Actor();
                    actor.Id = obj.Property("id").Value.ToObject<int>();
                    actor.BattlerName = obj.Property("battlerName").Value.ToObject<string>();
                    actor.CharacterIndex = obj.Property("characterIndex").Value.ToObject<int>();
                    actor.CharacterName = obj.Property("characterName").Value.ToObject<string>();
                    actor.ClassId = obj.Property("classId").Value.ToObject<int>();
                    actor.Equips = obj.Property("equips").Value.ToObject<List<int>>();
                    actor.FaceIndex = obj.Property("faceIndex").Value.ToObject<int>();
                    actor.FaceName = obj.Property("faceName").Value.ToObject<string>();
                    actor.InitialLevel = obj.Property("initialLevel").Value.ToObject<int>();
                    actor.MaxLevel = obj.Property("maxLevel").Value.ToObject<int>();
                    actor.Name = obj.Property("name").Value.ToObject<string>();
                    actor.Nickname = obj.Property("nickname").Value.ToObject<string>();
                    actor.Note = obj.Property("note").Value.ToObject<string>();
                    actor.Profile = obj.Property("profile").Value.ToObject<string>();
                    actor.Traits = obj.Property("traits").Value.ToObject<List<Trait>>();
                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultActorProperties.Contains(property.Name))
                        {
                            actor.Properties.Add(property.Name, property.Value.ToObject(typeof(object)));
                        }
                    }
                    Actors.Add(actor);
                }
                else
                {
                    Actors.Add(null);
                }
            }
            return Actors.Count;
        }

        /// <summary>
        /// Save the list of currently loaded actors to the specified file.
        /// </summary>
        /// <param name="file">file to save to</param>
        /// <param name="compact">value indicating whether the data should be formatted or left unformatted. Defaults to unformatted.</param>
        /// <returns>true if the data is successfully saved, false if an error occurs</returns>
        public static bool SaveActors(string file, bool compact = true)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                StringWriter stringWriter = new StringWriter(stringBuilder);


                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(stringWriter))
                {
                    writer.Formatting = Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (Actor actor in Actors)
                    {
                        // If the object isn't a null object we write it.
                        if (actor != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(actor.Id);
                            writer.WritePropertyName("battlerName");
                            writer.WriteValue(actor.BattlerName);
                            writer.WritePropertyName("characterIndex");
                            writer.WriteValue(actor.CharacterIndex);
                            writer.WritePropertyName("characterName");
                            writer.WriteValue(actor.CharacterName);
                            writer.WritePropertyName("classId");
                            writer.WriteValue(actor.ClassId);
                            writer.WritePropertyName("equips");

                            // We have to serialize the array of equips separately.
                            serializer.Serialize(tokenWriter, actor.Equips);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();


                            writer.WritePropertyName("faceIndex");
                            writer.WriteValue(actor.FaceIndex);
                            writer.WritePropertyName("faceName");
                            writer.WriteValue(actor.FaceName);
                            writer.WritePropertyName("traits");

                            // We have to serialize the traits separately.
                            serializer.Serialize(tokenWriter, actor.Traits);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("initialLevel");
                            writer.WriteValue(actor.InitialLevel);
                            writer.WritePropertyName("maxLevel");
                            writer.WriteValue(actor.MaxLevel);
                            writer.WritePropertyName("name");
                            writer.WriteValue(actor.Name);
                            writer.WritePropertyName("nickname");
                            writer.WriteValue(actor.Nickname);
                            writer.WritePropertyName("note");
                            writer.WriteValue(actor.Note);
                            writer.WritePropertyName("profile");
                            writer.WriteValue(actor.Profile);
                            foreach (KeyValuePair<string, object> kvp in actor.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();
                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, stringBuilder.ToString());
                return true;
            }
            catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
                
            }
        }

        /// <summary>
        /// Loaded animations from the specified file into the DataManager.Animations list.
        /// The list is cleared before loading.
        /// </summary>
        /// <param name="file">file to load from</param>
        /// <returns>the amount of animations loaded, or -1 if an error occurrs.</returns>
        public static int LoadAnimations(string file)
        {
            // Clear the currently loaded Animations.
            Animations.Clear();

            if (!File.Exists(file))
            {
#if LOG_ERRORS
                log.ErrorFormat("Failed to load Animations. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load Animations. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

            JArray animationsArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in animationsArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    Animation animation = new Animation();
                    animation.Id = obj.Property("id").Value.ToObject<int>();
                    animation.Animation1Hue = obj.Property("animation1Hue").Value.ToObject<int>();
                    animation.Animation1Name = obj.Property("animation1Name").Value.ToObject<string>();
                    animation.Animation2Hue = obj.Property("animation2Hue").Value.ToObject<int>();
                    animation.Animation2Name = obj.Property("animation2Name").Value.ToObject<string>();
                    animation.Frames = new List<List<AnimationCell>>();

                    foreach (JArray frame in obj.Property("frames").Children<JArray>().Children())
                    {
                        if (frame.HasValues)
                        {
                            List<AnimationCell> cells = new List<AnimationCell>();
                            foreach (JArray cell in frame.Children<JArray>())
                            {
                                if (cell.HasValues)
                                {
                                    AnimationCell cellData = new AnimationCell();
                                    cellData.Pattern = cell[0].ToObject<int>();
                                    cellData.X = cell[1].ToObject<int>();
                                    cellData.Y = cell[2].ToObject<int>();
                                    cellData.Scale = cell[3].ToObject<int>();
                                    cellData.Rotation = cell[4].ToObject<int>();
                                    cellData.Mirror = cell[5].ToObject<int>();
                                    cellData.Opacity = cell[6].ToObject<int>();
                                    cellData.Blend = cell[7].ToObject<int>();
                                    cells.Add(cellData);
                                }
                                else
                                {
                                    cells.Add(null);
                                }
                            }
                            animation.Frames.Add(cells);
                        }
                        else
                        {
                            animation.Frames.Add(null);
                        }
                    }

                    animation.Name = obj.Property("name").Value.ToObject<string>();
                    animation.Position = obj.Property("position").Value.ToObject<int>();
                    animation.Timings = obj.Property("timings").Value.ToObject<List<AnimationTiming>>();
                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultAnimationProperties.Contains(property.Name))
                        {
                            animation.Properties.Add(property.Name, property.Value.ToObject(typeof(object)));
                        }
                    }
                    Animations.Add(animation);
                }
                else
                {
                    Animations.Add(null);
                }
            }
            return Animations.Count;
        }

        /// <summary>
        /// Saves the currently loaded <see cref="Animations"/> to a file.
        /// </summary>
        /// <param name="file">file to save to.</param>
        /// <param name="compact">value to indicate whether or not the serialized JSON string is formatted or not.</param>
        /// <returns>a boolean value that indicates whether or not the data was saved successfully.</returns>
        public static bool SaveAnimations(string file, bool compact = true)
        {
            try
            {
                StringBuilder jsonBuilder = new StringBuilder();
                StringWriter jsonStrWriter = new StringWriter(jsonBuilder);
                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(jsonStrWriter))
                {
                    writer.Formatting = compact ? Formatting.None : Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (Animation animation in Animations)
                    {
                        // If the object isn't a null object we write it.
                        if (animation != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(animation.Id);
                            writer.WritePropertyName("animation1Hue");
                            writer.WriteValue(animation.Animation1Hue);
                            writer.WritePropertyName("animation1Name");
                            writer.WriteValue(animation.Animation1Name);
                            writer.WritePropertyName("animation2Hue");
                            writer.WriteValue(animation.Animation2Hue);
                            writer.WritePropertyName("animation2Name");
                            writer.WriteValue(animation.Animation2Name);

                            // We have to serialize the array of frames separately, because we use a custom .NET Object
                            writer.WritePropertyName("frames");
                            writer.WriteStartArray();
                            foreach (List<AnimationCell> frame in animation.Frames)
                            {
                                writer.WriteStartArray();
                                if (frame != null)
                                {
                                    foreach (AnimationCell cell in frame)
                                    {
                                        if (cell != null)
                                        {
                                            writer.WriteStartArray();
                                            writer.WriteValue(cell.Pattern);
                                            writer.WriteValue(cell.X);
                                            writer.WriteValue(cell.Y);
                                            writer.WriteValue(cell.Scale);
                                            writer.WriteValue(cell.Rotation);
                                            writer.WriteValue(cell.Mirror);
                                            writer.WriteValue(cell.Opacity);
                                            writer.WriteValue(cell.Blend);
                                            writer.WriteEndArray();
                                        }
                                        else
                                        {
                                            writer.WriteStartArray();
                                            writer.WriteEndArray();
                                        }
                                    }

                                }
                                writer.WriteEndArray();
                            }
                            writer.WriteEndArray();

                            writer.WritePropertyName("name");
                            writer.WriteValue(animation.Name);
                            writer.WritePropertyName("position");
                            writer.WriteValue(animation.Position);

                            // We have to serialize the timings separately.
                            writer.WritePropertyName("timings");
                            serializer.Serialize(tokenWriter, animation.Timings);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            // Itterate through every non-default property and serialize them, then add to the JSON object.
                            foreach (KeyValuePair<string, object> kvp in animation.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();

                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, jsonBuilder.ToString());
                return true;
            }
            catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int LoadArmors(string file)
        {
            // Clear the currently loaded Armors.
            Armors.Clear();

            if (!File.Exists(file))
            {
#if LOG_ERRORS

                log.ErrorFormat("Failed to load Armors. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load Armors. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

            JArray armorsArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in armorsArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    Armor armor = new Armor();
                    armor.Id = obj.Property("id").Value.ToObject<int>();
                    armor.ATypeId = obj.Property("atypeId").Value.ToObject<int>();
                    armor.Description = obj.Property("description").Value.ToObject<string>();
                    armor.ETypeId = obj.Property("etypeId").Value.ToObject<int>();
                    armor.Traits = obj.Property("traits").Value.ToObject<List<Trait>>();
                    armor.IconIndex = obj.Property("iconIndex").Value.ToObject<int>();
                    armor.Name = obj.Property("name").Value.ToObject<string>();
                    armor.Note = obj.Property("note").Value.ToObject<string>();
                    armor.Parameters = obj.Property("params").Value.ToObject<List<int>>();
                    armor.Price = obj.Property("price").Value.ToObject<int>();

                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultArmorProperties.Contains(property.Name))
                        {
                            armor.Properties.Add(property.Name, property.Value.ToObject<object>());
                        }
                    }
                    Armors.Add(armor);
                }
                else
                {
                    Armors.Add(null);
                }
            }
            return Armors.Count;

        }

        public static bool SaveArmors(string file, bool compact = true)
        {
            try
            {
                StringBuilder jsonBuilder = new StringBuilder();
                StringWriter jsonStrWriter = new StringWriter(jsonBuilder);
                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(jsonStrWriter))
                {
                    writer.Formatting = compact ? Formatting.None : Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (Armor armor in Armors)
                    {
                        // If the object isn't a null object we write it.
                        if (armor != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(armor.Id);
                            writer.WritePropertyName("atypeId");
                            writer.WriteValue(armor.ATypeId);
                            writer.WritePropertyName("description");
                            writer.WriteValue(armor.Description);
                            writer.WritePropertyName("etypeId");
                            writer.WriteValue(armor.ETypeId);

                            // Traits need to be serialized as its own token.
                            writer.WritePropertyName("traits");
                            serializer.Serialize(tokenWriter, armor.Traits);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("iconIndex");
                            writer.WriteValue(armor.IconIndex);
                            writer.WritePropertyName("name");
                            writer.WriteValue(armor.Name);
                            writer.WritePropertyName("note");
                            writer.WriteValue(armor.Note);

                            // Params need to be serialized as its own token.
                            writer.WritePropertyName("params");
                            serializer.Serialize(tokenWriter, armor.Parameters);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            // Itterate through every non-default property and serialize them, then add to the JSON object.
                            foreach (KeyValuePair<string, object> kvp in armor.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();

                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, jsonBuilder.ToString());
                return true;
            }
          catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        
        }
    

        public static int LoadClasses(string file)
        {
            // Clear the currently loaded Classes.
            Classes.Clear();

            if (!File.Exists(file))
            {
#if LOG_ERRORS

                log.ErrorFormat("Failed to load Classes. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load Classes. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

            JArray classesArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in classesArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    // new class object
                    Class nClass = new Class();
                    nClass.Id = obj.Property("id").Value.ToObject<int>();
                    nClass.ExpParams = obj.Property("expParams").Value.ToObject<List<int>>();
                    nClass.Traits = obj.Property("traits").Value.ToObject<List<Trait>>();
                    nClass.Learnings = obj.Property("learnings").Value.ToObject<List<Learning>>();
                    nClass.Name = obj.Property("name").Value.ToObject<string>();
                    nClass.Note = obj.Property("note").Value.ToObject<string>();
                    nClass.Parameters = obj.Property("params").Value.ToObject<List<List<int>>>();
                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultClassProperties.Contains(property.Name))
                        {
                            nClass.Properties.Add(property.Name, property.Value.ToObject<object>());
                        }
                    }
                    Classes.Add(nClass);

                }
                else
                {
                    Classes.Add(null);
                }
            }
            return Classes.Count;
        }

        public static bool SaveClasses(string file, bool compact = true)
        {
            try
            {
                StringBuilder jsonBuilder = new StringBuilder();
                StringWriter jsonStrWriter = new StringWriter(jsonBuilder);
                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(jsonStrWriter))
                {
                    writer.Formatting = compact ? Formatting.None : Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (Class classObj in Classes)
                    {
                        // If the object isn't a null object we write it.
                        if (classObj != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(classObj.Id);

                            // Exp Params need to be serialized as its own token.
                            writer.WritePropertyName("expParams");
                            serializer.Serialize(tokenWriter, classObj.ExpParams);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            // Traits need to be serialized as its own token.
                            writer.WritePropertyName("traits");
                            serializer.Serialize(tokenWriter, classObj.Traits);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            // Learnings need to be serialized as its own token.
                            writer.WritePropertyName("learnings");
                            serializer.Serialize(tokenWriter, classObj.Learnings);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();
                            
                            writer.WritePropertyName("name");
                            writer.WriteValue(classObj.Name);
                            writer.WritePropertyName("note");
                            writer.WriteValue(classObj.Note);

                            // Params need to be serialized as its own token.
                            writer.WritePropertyName("params");
                            serializer.Serialize(tokenWriter, classObj.Parameters);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            // Itterate through every non-default property and serialize them, then add to the JSON object.
                            foreach (KeyValuePair<string, object> kvp in classObj.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();

                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, jsonBuilder.ToString());
                return true;
            }
            catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int LoadEnemies(string file)
        {
            // Clear the list of loaded enemies.
            Enemies.Clear();

            if (!File.Exists(file))
            {
#if LOG_ERRORS

                log.ErrorFormat("Failed to load Enemies. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load Enemies. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

            JArray enemiesArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in enemiesArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    // New Enemy Object
                    Enemy enemy = new Enemy();
                    enemy.Id = obj.Property("id").Value.ToObject<int>();
                    enemy.Actions = obj.Property("actions").Value.ToObject<List<EnemyAction>>();
                    enemy.BattlerHue = obj.Property("battlerHue").Value.ToObject<int>();
                    enemy.BattlerName = obj.Property("battlerName").Value.ToObject<string>();
                    enemy.DropItems = obj.Property("dropItems").Value.ToObject<List<DropItem>>();
                    enemy.Exp = obj.Property("exp").Value.ToObject<int>();
                    enemy.Traits = obj.Property("traits").Value.ToObject<List<Trait>>();
                    enemy.Gold = obj.Property("gold").Value.ToObject<int>();
                    enemy.Name = obj.Property("name").Value.ToObject<string>();
                    enemy.Note = obj.Property("note").Value.ToObject<string>();
                    enemy.Parameters = obj.Property("params").Value.ToObject<List<int>>();
                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultEnemyProperties.Contains(property.Name))
                        {
                            enemy.Properties.Add(property.Name, property.Value.ToObject<object>());
                        }
                    }
                    Enemies.Add(enemy);

                }
                else
                {
                    Enemies.Add(null);
                }
            }
            return Enemies.Count;
        }

        public static bool SaveEnemies(string file, bool compact = true)
        {
            try
            {
                StringBuilder jsonBuilder = new StringBuilder();
                StringWriter jsonStrWriter = new StringWriter(jsonBuilder);
                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(jsonStrWriter))
                {
                    writer.Formatting = compact ? Formatting.None : Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (Enemy enemy in Enemies)
                    {
                        // If the object isn't a null object we write it.
                        if (enemy != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(enemy.Id);

                            // Actions need to be serialized as its own token.
                            writer.WritePropertyName("actions");
                            serializer.Serialize(tokenWriter, enemy.Actions);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("battlerHue");
                            writer.WriteValue(enemy.BattlerHue);
                            writer.WritePropertyName("battlerName");
                            writer.WriteValue(enemy.BattlerName);


                            // Drop Items need to be serialized as its own token.
                            writer.WritePropertyName("dropItems");
                            serializer.Serialize(tokenWriter, enemy.DropItems);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("exp");
                            writer.WriteValue(enemy.Exp);
                           
                            // Traits need to be serialized as its own token.
                            writer.WritePropertyName("traits");
                            serializer.Serialize(tokenWriter, enemy.Traits);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();
                           
                            writer.WritePropertyName("gold");
                            writer.WriteValue(enemy.Gold); 
                            writer.WritePropertyName("name");
                            writer.WriteValue(enemy.Name);
                            writer.WritePropertyName("note");
                            writer.WriteValue(enemy.Note);

                            // Params need to be serialized as its own token.
                            writer.WritePropertyName("params");
                            serializer.Serialize(tokenWriter, enemy.Parameters);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            // Itterate through every non-default property and serialize them, then add to the JSON object.
                            foreach (KeyValuePair<string, object> kvp in enemy.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();

                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, jsonBuilder.ToString());
                return true;
            }
            catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int LoadItems(string file)
        {
            // Clear the currently loaded Items.
            Items.Clear();

            if (!File.Exists(file))
            {
#if LOG_ERRORS

                log.ErrorFormat("Failed to load Items. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load Items. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

            JArray itemsArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in itemsArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    // New Item Object
                    Item item = new Item();
                    item.Id = obj.Property("id").Value.ToObject<int>();
                    item.AnimationId = obj.Property("animationId").Value.ToObject<int>();
                    item.Consumable = obj.Property("consumable").Value.ToObject<bool>();
                    item.Damage = obj.Property("damage").Value.ToObject<Damage>();
                    item.Description = obj.Property("description").Value.ToObject<string>();
                    item.Effects = obj.Property("effects").Value.ToObject<List<Effect>>();
                    item.HitType = obj.Property("hitType").Value.ToObject<int>();
                    item.IconIndex = obj.Property("iconIndex").Value.ToObject<int>();
                    item.ITypeId = obj.Property("itypeId").Value.ToObject<int>();
                    item.Name = obj.Property("name").Value.ToObject<string>();
                    item.Note = obj.Property("note").Value.ToObject<string>();
                    item.Occasion = obj.Property("occasion").Value.ToObject<int>();
                    item.Price = obj.Property("price").Value.ToObject<int>();
                    item.Repeats = obj.Property("repeats").Value.ToObject<int>();
                    item.Scope = obj.Property("scope").Value.ToObject<int>();
                    item.Speed = obj.Property("speed").Value.ToObject<int>();
                    item.SuccessRate = obj.Property("successRate").Value.ToObject<int>();
                    item.TpGain = obj.Property("tpGain").Value.ToObject<int>();
                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultItemProperties.Contains(property.Name))
                        {
                            item.Properties.Add(property.Name, property.Value.ToObject<object>());
                        }
                    }
                    Items.Add(item);

                }
                else
                {
                    Items.Add(null);
                }
            }
            return Items.Count;
        }

        public static bool SaveItems(string file, bool compact = true)
        {
            try
            {
                StringBuilder jsonBuilder = new StringBuilder();
                StringWriter jsonStrWriter = new StringWriter(jsonBuilder);
                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(jsonStrWriter))
                {
                    writer.Formatting = compact ? Formatting.None : Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (Item item in Items)
                    {
                        // If the object isn't a null object we write it.
                        if (item != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(item.Id);
                            writer.WritePropertyName("animationId");
                            writer.WriteValue(item.AnimationId);
                            writer.WritePropertyName("consumable");
                            writer.WriteValue(item.Consumable);
                            
                            // Damage object needs to be serialized as its own token.
                            writer.WritePropertyName("damage");
                            serializer.Serialize(tokenWriter, item.Damage);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("description");
                            writer.WriteValue(item.Description);
                            
                            // Effects need to be serialized as its own token.
                            writer.WritePropertyName("effects");
                            serializer.Serialize(tokenWriter, item.Effects);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("hitType");
                            writer.WriteValue(item.HitType);
                            writer.WritePropertyName("iconIndex");
                            writer.WriteValue(item.IconIndex);
                            writer.WritePropertyName("itypeId");
                            writer.WriteValue(item.ITypeId);
                            writer.WritePropertyName("name");
                            writer.WriteValue(item.Name);
                            writer.WritePropertyName("note");
                            writer.WriteValue(item.Note);
                            writer.WritePropertyName("occasion");
                            writer.WriteValue(item.Occasion);
                            writer.WritePropertyName("price");
                            writer.WriteValue(item.Price);
                            writer.WritePropertyName("repeats");
                            writer.WriteValue(item.Repeats);
                            writer.WritePropertyName("scope");
                            writer.WriteValue(item.Scope);
                            writer.WritePropertyName("speed");
                            writer.WriteValue(item.Speed);
                            writer.WritePropertyName("successRate");
                            writer.WriteValue(item.SuccessRate);
                            writer.WritePropertyName("tpGain");
                            writer.WriteValue(item.TpGain);
                            // Itterate through every non-default property and serialize them, then add to the JSON object.
                            foreach (KeyValuePair<string, object> kvp in item.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();

                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, jsonBuilder.ToString());
                return true;
            }
            catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int LoadSkills(string file)
        {
            // Clear the currently loaded Skills.
            Skills.Clear();

            if (!File.Exists(file))
            {
#if LOG_ERRORS

                log.ErrorFormat("Failed to load Skills. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load Skills. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

            JArray skillsArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in skillsArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    // New Skill Object
                    Skill skill = new Skill();
                    skill.Id = obj.Property("id").Value.ToObject<int>();
                    skill.AnimationId = obj.Property("animationId").Value.ToObject<int>();
                    skill.Damage = obj.Property("damage").Value.ToObject<Damage>();
                    skill.Description = obj.Property("description").Value.ToObject<string>();
                    skill.Effects = obj.Property("effects").Value.ToObject<List<Effect>>();
                    skill.HitType = obj.Property("hitType").Value.ToObject<int>();
                    skill.IconIndex = obj.Property("iconIndex").Value.ToObject<int>();
                    skill.STypeId = obj.Property("stypeId").Value.ToObject<int>();
                    skill.Name = obj.Property("name").Value.ToObject<string>();
                    skill.Note = obj.Property("note").Value.ToObject<string>();
                    skill.Occasion = obj.Property("occasion").Value.ToObject<int>();
                    skill.Repeats = obj.Property("repeats").Value.ToObject<int>();
                    skill.Scope = obj.Property("scope").Value.ToObject<int>();
                    skill.Speed = obj.Property("speed").Value.ToObject<int>();
                    skill.SuccessRate = obj.Property("successRate").Value.ToObject<int>();
                    skill.TpGain = obj.Property("tpGain").Value.ToObject<int>();
                    skill.Message1 = obj.Property("message1").Value.ToObject<string>();
                    skill.Message2 = obj.Property("message2").Value.ToObject<string>();
                    skill.MpCost = obj.Property("mpCost").Value.ToObject<int>();
                    skill.TpCost = obj.Property("tpCost").Value.ToObject<int>();
                    skill.RequiredWtypeId1 = obj.Property("requiredWtypeId1").Value.ToObject<int>();
                    skill.RequiredWtypeId2 = obj.Property("requiredWtypeId2").Value.ToObject<int>();
                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultSkillProperties.Contains(property.Name))
                        {
                            skill.Properties.Add(property.Name, property.Value.ToObject<object>());
                        }
                    }
                    Skills.Add(skill);

                }
                else
                {
                    Skills.Add(null);
                }
            }
            return Skills.Count;
        }

        public static bool SaveSkills(string file, bool compact = true)
        {
            try
            {
                StringBuilder jsonBuilder = new StringBuilder();
                StringWriter jsonStrWriter = new StringWriter(jsonBuilder);
                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(jsonStrWriter))
                {
                    writer.Formatting = compact ? Formatting.None : Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (Skill skill in Skills)
                    {
                        // If the object isn't a null object we write it.
                        if (skill != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(skill.Id);
                            writer.WritePropertyName("animationId");
                            writer.WriteValue(skill.AnimationId);

                            // Damage object needs to be serialized as its own token.
                            writer.WritePropertyName("damage");
                            serializer.Serialize(tokenWriter, skill.Damage);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("description");
                            writer.WriteValue(skill.Description);

                            // Effects need to be serialized as its own token.
                            writer.WritePropertyName("effects");
                            serializer.Serialize(tokenWriter, skill.Effects);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("hitType");
                            writer.WriteValue(skill.HitType);
                            writer.WritePropertyName("iconIndex");
                            writer.WriteValue(skill.IconIndex);
                            writer.WritePropertyName("message1");
                            writer.WriteValue(skill.Message1);
                            writer.WritePropertyName("message2");
                            writer.WriteValue(skill.Message2);
                            writer.WritePropertyName("mpCost");
                            writer.WriteValue(skill.MpCost);
                            writer.WritePropertyName("name");
                            writer.WriteValue(skill.Name);
                            writer.WritePropertyName("note");
                            writer.WriteValue(skill.Note);
                            writer.WritePropertyName("occasion");
                            writer.WriteValue(skill.Occasion);
                            writer.WritePropertyName("repeats");
                            writer.WriteValue(skill.Repeats);
                            writer.WritePropertyName("requiredWtypeId1");
                            writer.WriteValue(skill.RequiredWtypeId1);
                            writer.WritePropertyName("requiredWtypeId2");
                            writer.WriteValue(skill.RequiredWtypeId2);
                            writer.WritePropertyName("scope");
                            writer.WriteValue(skill.Scope);
                            writer.WritePropertyName("speed");
                            writer.WriteValue(skill.Speed);
                            writer.WritePropertyName("stypeId");
                            writer.WriteValue(skill.STypeId);
                            writer.WritePropertyName("successRate");
                            writer.WriteValue(skill.SuccessRate);
                            writer.WritePropertyName("tpCost");
                            writer.WriteValue(skill.TpCost);
                            writer.WritePropertyName("tpGain");
                            writer.WriteValue(skill.TpGain);
                            // Itterate through every non-default property and serialize them, then add to the JSON object.
                            foreach (KeyValuePair<string, object> kvp in skill.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();

                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, jsonBuilder.ToString());
                return true;
            }
            catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int LoadStates(string file)
        {
            // Clear the currently loaded states.
            States.Clear();

            if (!File.Exists(file))
            {
#if LOG_ERRORS

                log.ErrorFormat("Failed to load States. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load States. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

            JArray statesArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in statesArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    State state = new State();
                    state.Id = obj.Property("id").Value.ToObject<int>();
                    state.AutoRemovalTiming = obj.Property("autoRemovalTiming").Value.ToObject<int>();
                    state.ChanceByDamage = obj.Property("chanceByDamage").Value.ToObject<int>();
                    state.Description = obj.Property("description").Value.ToObject<string>();
                    state.IconIndex = obj.Property("iconIndex").Value.ToObject<int>();
                    state.MaxTurns = obj.Property("maxTurns").Value.ToObject<int>();
                    state.Message1 = obj.Property("message1").Value.ToObject<string>();
                    state.Message2 = obj.Property("message2").Value.ToObject<string>();
                    state.Message3 = obj.Property("message3").Value.ToObject<string>();
                    state.Message4 = obj.Property("message4").Value.ToObject<string>();
                    state.MinTurns = obj.Property("minTurns").Value.ToObject<int>();
                    state.Motion = obj.Property("motion").Value.ToObject<int>();
                    state.Name = obj.Property("name").Value.ToObject<string>();
                    state.Note = obj.Property("note").Value.ToObject<string>();
                    state.Overlay = obj.Property("overlay").Value.ToObject<int>();
                    state.Priority = obj.Property("priority").Value.ToObject<int>();
                    state.ReleaseByDamage = obj.Property("releaseByDamage").Value.ToObject<bool>();
                    state.RemoveAtBattleEnd = obj.Property("removeAtBattleEnd").Value.ToObject<bool>();
                    state.RemoveByDamage = obj.Property("removeByDamage").Value.ToObject<bool>();
                    state.RemoveByRestriction = obj.Property("removeByRestriction").Value.ToObject<bool>();
                    state.RemoveByWalking = obj.Property("removeByWalking").Value.ToObject<bool>();
                    state.Restriction = obj.Property("restriction").Value.ToObject<int>();
                    state.StepsToRemove = obj.Property("stepsToRemove").Value.ToObject<int>();
                    state.Traits = obj.Property("traits").Value.ToObject<List<Trait>>();
                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultStateProperties.Contains(property.Name))
                        {
                            state.Properties.Add(property.Name, property.Value.ToObject<object>());
                        }
                    }
                    States.Add(state);

                }
                else
                {
                    States.Add(null);
                }
            }
            return States.Count;
        }
        
        public static bool SaveStates(string file, bool compact = true)
        {
            try
            {
                StringBuilder jsonBuilder = new StringBuilder();
                StringWriter jsonStrWriter = new StringWriter(jsonBuilder);
                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(jsonStrWriter))
                {
                    writer.Formatting = compact ? Formatting.None : Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (State state in States)
                    {
                        // If the object isn't a null object we write it.
                        if (state != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(state.Id);
                            writer.WritePropertyName("autoRemovalTiming");
                            writer.WriteValue(state.AutoRemovalTiming);
                            writer.WritePropertyName("chanceByDamage");
                            writer.WriteValue(state.Description);
                            writer.WritePropertyName("iconIndex");
                            writer.WriteValue(state.IconIndex);
                            writer.WritePropertyName("maxTurns");
                            writer.WriteValue(state.MaxTurns);
                            writer.WritePropertyName("message1");
                            writer.WriteValue(state.Message1);
                            writer.WritePropertyName("message2");
                            writer.WriteValue(state.Message2);
                            writer.WritePropertyName("message3");
                            writer.WriteValue(state.Message3);
                            writer.WritePropertyName("message4");
                            writer.WriteValue(state.Message4);
                            writer.WritePropertyName("minTurns");
                            writer.WriteValue(state.MinTurns);
                            writer.WritePropertyName("motion");
                            writer.WriteValue(state.Motion);
                            writer.WritePropertyName("name");
                            writer.WriteValue(state.Name);
                            writer.WritePropertyName("note");
                            writer.WriteValue(state.Note);
                            writer.WritePropertyName("overlay");
                            writer.WriteValue(state.Overlay);
                            writer.WritePropertyName("priority");
                            writer.WriteValue(state.Priority);
                            writer.WritePropertyName("releaseByDamage");
                            writer.WriteValue(state.ReleaseByDamage);
                            writer.WritePropertyName("removeAtBattleEnd");
                            writer.WriteValue(state.RemoveAtBattleEnd);
                            writer.WritePropertyName("removeByDamage");
                            writer.WriteValue(state.RemoveByDamage);
                            writer.WritePropertyName("removeByRestriction");
                            writer.WriteValue(state.RemoveByRestriction);
                            writer.WritePropertyName("removeByWalking");
                            writer.WriteValue(state.RemoveByWalking);
                            writer.WritePropertyName("restriction");
                            writer.WriteValue(state.Restriction);
                            writer.WritePropertyName("stepsToRemove");
                            writer.WriteValue(state.StepsToRemove);
                            writer.WritePropertyName("description");
                            writer.WriteValue(state.Description);

                            // Traits need to be serialized as its own token.
                            writer.WritePropertyName("traits");
                            serializer.Serialize(tokenWriter, state.Traits);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            // Itterate through every non-default property and serialize them, then add to the JSON object.
                            foreach (KeyValuePair<string, object> kvp in state.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();

                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, jsonBuilder.ToString());
                return true;
            }
            catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int LoadTilesets(string file)
        {
            // Clear the currently loaded Tilesets.
            Tilesets.Clear();

            if (!File.Exists(file))
            {
#if LOG_ERRORS

                log.ErrorFormat("Failed to load Tilesets. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load Tilesets. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

            JArray tilesetsArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in tilesetsArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    Tileset tileset = new Tileset();
                    tileset.Id = obj.Property("id").Value.ToObject<int>();
                    tileset.Flags = obj.Property("flags").Value.ToObject<List<int>>();
                    tileset.Mode = obj.Property("mode").Value.ToObject<int>();
                    tileset.Name = obj.Property("name").Value.ToObject<string>();
                    tileset.Note = obj.Property("note").Value.ToObject<string>();
                    tileset.TilesetNames = obj.Property("tilesetNames").Value.ToObject<List<string>>();
                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultTilesetProperties.Contains(property.Name))
                        {
                            tileset.Properties.Add(property.Name, property.Value.ToObject<object>());
                        }
                    }
                    Tilesets.Add(tileset);

                }
                else
                {
                    Tilesets.Add(null);
                }
            }
            return Tilesets.Count;
        }

        public static bool SaveTilesets(string file, bool compact = true)
        {
            try
            {
                StringBuilder jsonBuilder = new StringBuilder();
                StringWriter jsonStrWriter = new StringWriter(jsonBuilder);
                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(jsonStrWriter))
                {
                    writer.Formatting = compact ? Formatting.None : Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (Tileset tileset in Tilesets)
                    {
                        // If the object isn't a null object we write it.
                        if (tileset != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(tileset.Id);
                            
                            // Flags need to be serialized as its own token.
                            writer.WritePropertyName("flags");
                            serializer.Serialize(tokenWriter, tileset.Flags);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("mode");
                            writer.WriteValue(tileset.Mode);
                            writer.WritePropertyName("name");
                            writer.WriteValue(tileset.Name);
                            writer.WritePropertyName("note");
                            writer.WriteValue(tileset.Note);

                            // Tileset Names need to be serialized as its own token.
                            writer.WritePropertyName("tilesetNames");
                            serializer.Serialize(tokenWriter, tileset.TilesetNames);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            // Itterate through every non-default property and serialize them, then add to the JSON object.
                            foreach (KeyValuePair<string, object> kvp in tileset.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();

                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, jsonBuilder.ToString());
                return true;
            }
            catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int LoadWeapons(string file)
        {
            // Clear the currently loaded Weapons.
            Weapons.Clear();

            if (!File.Exists(file))
            {
#if LOG_ERRORS

                log.ErrorFormat("Failed to load Weapons. File: {0} does not exists!", file);
#endif
                if (!HandleExceptions)
                {
                    throw new IOException(string.Format("Failed to load Weapons. The file {0} does not exists!", file));
                }
                else
                {
                    return -1;
                }
            }

            JArray weaponsArray = JArray.Parse(File.ReadAllText(file));
            foreach (JObject obj in weaponsArray.Children<JObject>())
            {
                if (obj.HasValues)
                {
                    Weapon weapon = new Weapon();
                    weapon.Id = obj.Property("id").Value.ToObject<int>();
                    weapon.WTypeId = obj.Property("wtypeId").Value.ToObject<int>();
                    weapon.Description = obj.Property("description").Value.ToObject<string>();
                    weapon.ETypeId = obj.Property("etypeId").Value.ToObject<int>();
                    weapon.Traits = obj.Property("traits").Value.ToObject<List<Trait>>();
                    weapon.IconIndex = obj.Property("iconIndex").Value.ToObject<int>();
                    weapon.Name = obj.Property("name").Value.ToObject<string>();
                    weapon.Note = obj.Property("note").Value.ToObject<string>();
                    weapon.Parameters = obj.Property("params").Value.ToObject<List<int>>();
                    weapon.Price = obj.Property("price").Value.ToObject<int>();

                    foreach (JProperty property in obj.Properties())
                    {
                        if (!defaultWeaponProperties.Contains(property.Name))
                        {
                            weapon.Properties.Add(property.Name, property.Value.ToObject<object>());
                        }
                    }
                    Weapons.Add(weapon);
                }
                else
                {
                    Weapons.Add(null);
                }
            }
            return Weapons.Count;

        }

        public static bool SaveWeapons(string file, bool compact = true)
        {
            try
            {
                StringBuilder jsonBuilder = new StringBuilder();
                StringWriter jsonStrWriter = new StringWriter(jsonBuilder);
                JsonSerializer serializer = new JsonSerializer();
                StringBuilder tokenBuilder = new StringBuilder();
                TextWriter tokenWriter = new StringWriter(tokenBuilder);

                using (JsonWriter writer = new JsonTextWriter(jsonStrWriter))
                {
                    writer.Formatting = compact ? Formatting.None : Formatting.Indented;
                    writer.WriteStartArray();
                    foreach (Weapon weapon in Weapons)
                    {
                        // If the object isn't a null object we write it.
                        if (weapon != null)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("id");
                            writer.WriteValue(weapon.Id);
                            writer.WritePropertyName("wtypeId");
                            writer.WriteValue(weapon.WTypeId);
                            writer.WritePropertyName("description");
                            writer.WriteValue(weapon.Description);
                            writer.WritePropertyName("etypeId");
                            writer.WriteValue(weapon.ETypeId);

                            // Traits need to be serialized as its own token.
                            writer.WritePropertyName("traits");
                            serializer.Serialize(tokenWriter, weapon.Traits);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            writer.WritePropertyName("iconIndex");
                            writer.WriteValue(weapon.IconIndex);
                            writer.WritePropertyName("name");
                            writer.WriteValue(weapon.Name);
                            writer.WritePropertyName("note");
                            writer.WriteValue(weapon.Note);

                            // Params need to be serialized as its own token.
                            writer.WritePropertyName("params");
                            serializer.Serialize(tokenWriter, weapon.Parameters);
                            writer.WriteRawValue(tokenBuilder.ToString());
                            tokenBuilder.Clear();

                            // Itterate through every non-default property and serialize them, then add to the JSON object.
                            foreach (KeyValuePair<string, object> kvp in weapon.Properties)
                            {
                                // We have no clue what these things could be, so we serialize it separately then write the raw json.
                                tokenBuilder.Clear();
                                writer.WritePropertyName(kvp.Key);
                                serializer.Serialize(tokenWriter, kvp.Value);
                                writer.WriteRawValue(tokenBuilder.ToString());
                            }
                            writer.WriteEndObject();

                        }
                        else
                        {
                            writer.WriteNull();
                        }
                    }
                    writer.WriteEndArray();
                }
                File.WriteAllText(file, jsonBuilder.ToString());
                return true;
            }
            catch (JsonException jsonEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(jsonEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException ioEx)
            {
#if LOG_ERRORS
                log.ErrorFormat(ioEx.Message);
#endif
                if (!HandleExceptions)
                {
                    throw;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
