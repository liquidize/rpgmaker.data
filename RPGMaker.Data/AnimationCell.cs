﻿// Copyright (c) 2016 Daniel Deptula
namespace RPGMaker.Data
{
    /// <summary>
    /// An Animation Cell object. This is an object that contains data on individual cells, or images used in <seealso cref="Animation"/> frames.
    /// </summary>
   public class AnimationCell
    {
        /// <summary>
        /// Pattern used by this cell.
        /// </summary>
        public int Pattern { get; set; }
        
        /// <summary>
        /// X location of this cell.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Y location of this cell.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Scale of this cell.
        /// </summary>
        public int Scale { get; set; }

        /// <summary>
        /// Rotation of this cell.
        /// </summary>
        public int Rotation { get; set; }

        /// <summary>
        /// The mirror value of this cell.
        /// </summary>
        public int Mirror { get; set; }

        /// <summary>
        /// The cells opacity.
        /// </summary>
        public int Opacity { get; set; }

        /// <summary>
        /// The blend type of this cell.
        /// </summary>
        public int Blend { get; set; }

        /// <summary>
        /// Returns a formatted string containing the values of this cell.
        /// </summary>
        /// <returns></returns>
       public override string ToString()
       {
           return string.Format("[{0},{1},{2},{3},{4},{5},{6},{7}]", Pattern, X, Y, Scale, Rotation, Mirror, Opacity,
               Blend);
       }
    }
}
