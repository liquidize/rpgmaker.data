﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
   public class Enemy
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "actions")]
        public List<EnemyAction> Actions { get; set; } = new List<EnemyAction>();
        [JsonProperty(PropertyName = "battlerHue")]
        public int BattlerHue { get; set; }
        [JsonProperty(PropertyName = "battlerName")]
        public string BattlerName { get; set; }
        [JsonProperty(PropertyName = "dropItems")]
        public List<DropItem> DropItems { get; set; } = new List<DropItem>();
        [JsonProperty(PropertyName = "exp")]
        public int Exp { get; set; }
        [JsonProperty(PropertyName = "traits")]
        public List<Trait> Traits { get; set; } = new List<Trait>();
        [JsonProperty(PropertyName = "gold")]
        public int Gold { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
        [JsonProperty(PropertyName = "params")]
        public List<int> Parameters { get; set; } = new List<int>();

        public Dictionary<string,object> Properties { get; set; } = new Dictionary<string, object>();

    }
}
