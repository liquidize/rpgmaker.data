﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
   public class EnemyAction
    {
        [JsonProperty(PropertyName = "conditionParam1")]
        public int ConditionParam1 { get; set; }
        [JsonProperty(PropertyName = "conditionParam2")]
        public double ConditionParam2 { get; set; }
        [JsonProperty(PropertyName = "conditionType")]
        public int ConditionType { get; set; }
        [JsonProperty(PropertyName = "rating")]
        public int Rating { get; set; }
        [JsonProperty(PropertyName = "skillId")]
        public int SkillId { get; set; }
    }
}
