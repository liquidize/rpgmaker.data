﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
   public class DropItem
    {
        [JsonProperty(PropertyName = "kind")]
        public int Kind { get; set; }
        [JsonProperty(PropertyName = "dataId")]
        public int DataId { get; set; }
        [JsonProperty(PropertyName = "denominator")]
        public int Denominator { get; set; }
    }
}
