﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
    public class Skill
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "animationId")]
        public int AnimationId { get; set; }
        [JsonProperty(PropertyName = "damage")]
        public Damage Damage { get; set; } = new Damage();
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "effects")]
        public List<Effect> Effects { get; set; } = new List<Effect>();
        [JsonProperty(PropertyName = "hitType")]
        public int HitType { get; set; }
        [JsonProperty(PropertyName = "iconIndex")]
        public int IconIndex { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
        [JsonProperty(PropertyName = "occasion")]
        public int Occasion { get; set; }
        [JsonProperty(PropertyName = "repeats")]
        public int Repeats { get; set; }
        [JsonProperty(PropertyName = "scope")]
        public int Scope { get; set; }
        [JsonProperty(PropertyName = "speed")]
        public int Speed { get; set; }
        [JsonProperty(PropertyName = "successRate")]
        public int SuccessRate { get; set; }
        [JsonProperty(PropertyName = "tpGain")]
        public int TpGain { get; set; }
        [JsonProperty(PropertyName = "message1")]
        public string Message1 { get; set; }
        [JsonProperty(PropertyName = "message2")]
        public string Message2 { get; set; }
        [JsonProperty(PropertyName = "mpCost")]
        public int MpCost { get; set; }
        [JsonProperty(PropertyName = "stypeId")]
        public int STypeId { get; set; }
        [JsonProperty(PropertyName = "tpCost")]
        public int TpCost { get; set; }
        [JsonProperty(PropertyName = "requiredWtypeId1")]
        public int RequiredWtypeId1 { get; set; }
        [JsonProperty(PropertyName = "requiredWtypeId2")]
        public int RequiredWtypeId2 { get; set; }
        public Dictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();
    }
}
