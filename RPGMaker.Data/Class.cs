﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
    public class Class
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "expParams")]
        public List<int> ExpParams { get; set; } = new List<int>();

        [JsonProperty(PropertyName = "traits")]
        public List<Trait> Traits { get; set; } = new List<Trait>();

        [JsonProperty(PropertyName = "learnings")]
        public List<Learning> Learnings { get; set; } = new List<Learning>();

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [JsonProperty(PropertyName = "params")]
        public List<List<int>> Parameters { get; set; } = new List<List<int>>();

        public Dictionary<string,object> Properties { get; set; } = new Dictionary<string, object>();
    }
}
