﻿// Copyright (c) 2016 Daniel Deptula
using System.Collections.Generic;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
    /// <summary>
    /// An object conatining data on an effect used to create flashes,tints, and sounds for <seealso cref="Animation"/>s.
    /// </summary>
    public class AnimationTiming
    {

        [JsonProperty(PropertyName = "flashColor")]
        public List<int> FlashColor { get; set; }

        [JsonProperty(PropertyName = "flashDuration")]
        public int FlashDuration { get; set; }

        [JsonProperty(PropertyName = "flashScope")]
        public int FlashScope { get; set; }

        [JsonProperty(PropertyName = "frame")]
        public int Frame { get; set; }

        [JsonProperty(PropertyName = "se")]
        public SoundEffect Se { get; set; }

        [JsonIgnore]
        public int? Conditions { get; set; }

        public override string ToString()
        {
            string str =
                string.Format(
                    "[FlashColor: R:{0} G:{1} B:{2}, FlashDuration: {3}, FlashScope: {4}, Frame: {5}, Se: {6}]",
                    FlashColor[0], FlashColor[1], FlashColor[3], FlashDuration, FlashScope, Frame, Se == null ? "" : Se.ToString());
            return str;
        }
    }
}
