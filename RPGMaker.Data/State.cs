﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
    public class State
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "autoRemovalTiming")]
        public int AutoRemovalTiming { get; set; }

        [JsonProperty(PropertyName = "chanceByDamage")]
        public int ChanceByDamage { get; set; }

        [JsonProperty(PropertyName = "iconIndex")]
        public int IconIndex { get; set; }

        [JsonProperty(PropertyName = "maxTurns")]
        public int MaxTurns { get; set; }

        [JsonProperty(PropertyName = "message1")]
        public string Message1 { get; set; }

        [JsonProperty(PropertyName = "message2")]
        public string Message2 { get; set; }

        [JsonProperty(PropertyName = "message3")]
        public string Message3 { get; set; }

        [JsonProperty(PropertyName = "message4")]
        public string Message4 { get; set; }

        [JsonProperty(PropertyName = "minTurns")]
        public int MinTurns { get; set; }

        [JsonProperty(PropertyName = "motion")]
        public int Motion { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [JsonProperty(PropertyName = "overlay")]
        public int Overlay { get; set; }

        [JsonProperty(PropertyName = "priority")]
        public int Priority { get; set; }

        [JsonProperty(PropertyName = "releaseByDamage")]
        public bool ReleaseByDamage { get; set; }

        [JsonProperty(PropertyName = "removeAtBattleEnd")]
        public bool RemoveAtBattleEnd { get; set; }

        [JsonProperty(PropertyName = "removeByDamage")]
        public bool RemoveByDamage { get; set; }

        [JsonProperty(PropertyName = "removeByRestriction")]
        public bool RemoveByRestriction { get; set; }

        [JsonProperty(PropertyName = "removeByWalking")]
        public bool RemoveByWalking { get; set; }

        [JsonProperty(PropertyName = "restriction")]
        public int Restriction { get; set; }

        [JsonProperty(PropertyName = "stepsToRemove")]
        public int StepsToRemove { get; set; }

        [JsonProperty(PropertyName = "traits")]
        public List<Trait> Traits { get; set; } = new List<Trait>();

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        public  Dictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();
    }
}
