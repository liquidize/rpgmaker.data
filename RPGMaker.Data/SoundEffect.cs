﻿// Copyright (c) 2016 Daniel Deptula
using Newtonsoft.Json;

namespace RPGMaker.Data
{
   public class SoundEffect
    {
        /// <summary>
        /// Name of the sound.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Pan value of the sound.
        /// </summary>
        [JsonProperty(PropertyName = "pan")]
        public int Pan { get; set; }

        /// <summary>
        /// Pitch value of the sound.
        /// </summary>
        [JsonProperty(PropertyName = "pitch")]
        public int Pitch { get; set; }

        /// <summary>
        /// Volume value of the sound.
        /// </summary>
        [JsonProperty(PropertyName = "volume")]
        public int Volume { get; set; }

        /// <summary>
        /// Returns a formatted string containing the values of this sound effect.
        /// </summary>
        /// <returns></returns>
       public override string ToString()
       {
           string str = string.Format("[Name: {0}, Pan: {1}, Pitch: {2}, Volume: {3}]", Name, Pan, Pitch, Volume);
           return str;
       }
    }
}
