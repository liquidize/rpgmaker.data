﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
   public class Effect
    {
        [JsonProperty(PropertyName = "code")]
        public int Code { get; set; }
        [JsonProperty(PropertyName = "dataId")]
        public int DataId { get; set; }
        [JsonProperty(PropertyName = "value1")]
        public int Value1 { get; set; }
        [JsonProperty(PropertyName = "value2")]
        public int Value2 { get; set; }
    }
}
