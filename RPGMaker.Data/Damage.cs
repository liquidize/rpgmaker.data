﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RPGMaker.Data
{
   public class Damage
    {
        [JsonProperty(PropertyName = "critical")]
        public bool Critical { get; set; }
        [JsonProperty(PropertyName = "elementId")]
        public int ElementId { get; set; }
        [JsonProperty(PropertyName = "formula")]
        public string Formula { get; set; }
        [JsonProperty(PropertyName = "type")]
        public int Type { get; set; }
        [JsonProperty(PropertyName = "variance")]
        public int Variance { get; set; }
    }
}
